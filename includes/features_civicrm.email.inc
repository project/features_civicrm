<?php
/**
 * @file
 * Module file for the features_cicivrm module, which enables
 * the capture and management of civicrm entities in Drupal.
 */

/**
 * Remove fields which should not be exported.
 */
function email_remove_fields(array $email) {
  return $email;
}
/**
 * Implements hook_features_export_options().
 */
function email_features_export_options() {
  $options = array();
  $api_email_api = features_civicrm_api('Email', 'get', array('contact_id' => 1));
  $options['email_1'] = $api_email_api['values'][1]['email'];
  return $options;
}

/**
 * Implements hook_features_export().
 */
function email_features_export($data, &$export, $module_name = '') {
  $export['dependencies']['civicrm'] = 'civicrm';
  $export['dependencies']['features_civicrm'] = 'features_civicrm';
  $export['dependencies']['features'] = 'features';
  $export['features']['email']['email_1'] = 'email_1';
  return array();
}

/**
 * Implements hook_features_export_render().
 */
function email_features_export_render($module_name, $data, $export = NULL) {
  $code = array();
  $code[] = '  $civicrm_email = array();';
  $code[] = '';
  $email_api_data = features_civicrm_api('Email', 'get', array('contact_id' => 1));
  $email_api_data = email_remove_fields($email_api_data['values'][1]);
  $email_code = features_var_export($email_api_data);
  $code[] = "  \$civicrm_email['email_1'] = {$email_code};";
  $code[] = '  return $civicrm_email;';
  $code = implode("\n", $code);
  return array('civicrm_default_email' => $code);
}

/**
 * Implements hook_features_rebuild().
 */
function email_features_rebuild($module) {
  email_features_revert($module);
}

/**
 * Implements hook_features_revert().
 */
function email_features_revert($module) {
  $civicrm_emails = module_invoke($module, 'civicrm_default_email');
  $email = reset($civicrm_emails);
  $existing_email = features_civicrm_api('Email', 'get', array('contact_id' => 1));
  // Merge with code_contact.
  $merged_email = array_merge_recursive_distinct($existing_email['values'][$existing_email['id']], $email);
  $api_created_email = features_civicrm_api('Email', 'create', $merged_contact);
}
